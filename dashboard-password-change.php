<?php include('inc/header.php'); ?>

<?php include('inc/admin-top.php'); ?>



<!-- dashboard-section start -->
<section id="dashboard-section" class="dashboard-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <?php include('inc/dashboard-menu.php'); ?>
      </div>

      <div class="col-lg-9">
        <div class="my-account-box">
          <div class="my-account-header">
            <h6>Password Change</h6>
          </div>
          <div class="my-account-body">
            <form action="#" class="eflux-login-form">
              <div class="row">
                <div class="col-lg-12">
                  <div class="input-item">
                    <label>Your Old Password</label>
                    <input type="password" name="password">
                  </div>
                </div>

                <div class="col-lg-12">
                  <div class="input-item">
                    <label>New Password</label>
                    <input type="password" name="password">
                  </div>
                </div>

                <div class="col-lg-12">
                  <div class="input-item">
                    <label>Confirm New Password</label>
                    <input type="password" name="password">
                  </div>
                </div>
              </div>

              <div>
                <button type="submit" class="submit">Save Changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- dashboard-section end -->


<?php include('inc/footer.php'); ?>