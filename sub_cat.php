<?php include('inc/header.php'); ?>
<section class="category-banner-section">
  <img src="assets/images/banner/banner-bg3.jpg" class="cat-banner" alt="">
  <!-- <div class="banner-content-area">
                    <div class="container">
                        <div class="banner-content">
                            <h6>Organic and fresh food</h6>
                            <h2>Get freshness delivered<br>on your doorstep.</h2>
                            <a href="#" class="banner-btn">Read More</a>
                        </div>
                    </div>
                </div> -->
</section>
<section class="sub-cats">
  <div class="category-container2 container">
    <div class="row">
      <div class="col-12 col-md-12">
        <div class="section-heading py-3">
          <h4 class="heading-title"><span class="heading-circle green"></span> Fish & Meat</h4>
        </div>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
      <div class="col-6 col-sm-4 col-md-3 col-lg-2">
        <a href="product-list.php" class="category-item">
          <img src="assets/images/category-product/04.jpg" alt="category image">
          <p>Meat and Fish</p>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('inc/footer.php'); ?>