<?php include('inc/header.php'); ?>
<section class="product-banner-section">
    <img src="assets/images/banner/banner-bg3.jpg" class="cat-banner" alt="">
    <!-- <div class="banner-content-area">
                    <div class="container">
                        <div class="banner-content">
                            <h6>Organic and fresh food</h6>
                            <h2>Get freshness delivered<br>on your doorstep.</h2>
                            <a href="#" class="banner-btn">Read More</a>
                        </div>
                    </div>
                </div> -->
</section>
<section class="page-content product-section">
    <div class="container">
        <div class="section-heading">
            <h2 class="heading-title"><span class="heading-circle"></span> Our Product</h2>
        </div>
    </div>
    <div class="container">
        <div class="rows">
            <div class="col-lg-s12">
                <div class="row product-list">
                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product-stock_out.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: block;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>

                    <div class="col-sm-6 col-lg-4 col-xl-3" style="display: none;">
                        <?php include('inc/product.php'); ?>
                    </div>
                    <div class="col-12 text-center mt-4">
                        <button class="loadMore">Load More</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('inc/footer.php'); ?>