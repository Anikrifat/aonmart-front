<?php include('inc/header.php'); ?>

<section class="login-section section-ptb">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-12">
        <div class="eflux-login-form-area">
          <form action="#" class="eflux-login-form">
            <div class="row">
              <div class="col-lg-6">
                <div class="input-item">
                  <label>First Name</label>
                  <input type="text" name="name" placeholder="First Name" />
                </div>
              </div>

              <div class="col-lg-6">
                <div class="input-item">
                  <label>Last Name</label>
                  <input type="text" name="name" placeholder="Last Name" />
                </div>
              </div>

              <div class="col-lg-6">
                <div class="input-item">
                  <label>Email</label>
                  <input type="email" name="email" placeholder="Email Address" />
                </div>
              </div>

              <div class="col-lg-6">
                <div class="input-item">
                  <label>Password</label>
                  <input type="password" name="website" placeholder="Password" />
                </div>
              </div>
            </div>

            <div>
              <button type="submit" class="submit">Create Account</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('inc/footer.php'); ?>