<?php include('inc/header.php'); ?>

<?php include('inc/admin-top.php'); ?>



<!-- dashboard-section start -->
<section id="dashboard-section" class="dashboard-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <?php include('inc/dashboard-menu.php'); ?>
      </div>

      <div class="col-lg-9">
        <div class="dashboard-body wishlist">
          <div class="wishlist-header">
            <h6>My Wishlist</h6>
          </div>
          <div class="wish-list-container">
            <div class="wishlist-item product-item d-flex align-items-center">
              <span class="close-item"><i class="fas fa-times"></i></span>
              <div class="thumb">
                <a onclick="openModal()"><img src="assets/images/products/cart/01.png" alt="products"></a>
              </div>
              <div class="product-content">
                <a href="product-detail.html" class="product-title">Daisy Cont Oil</a>
                <div class="product-cart-info">
                  1x 31b
                </div>
                <div class="product-price">
                  <del>$8.00</del><span class="ml-4">$5.00</span>
                </div>
                <div class="cart-btn-toggle" onclick="cartopen()">
                  <span class="cart-btn"><i class="fas fa-shopping-cart"></i> Cart</span>

                  <div class="price-btn">
                    <div class="price-increase-decrese-group d-flex">
                      <span class="decrease-btn">
                        <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">-
                        </button>
                      </span>
                      <input type="text" name="quantity" class="form-controls input-number" value="1">
                      <span class="increase">
                        <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">+
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="wishlist-item product-item d-flex align-items-center">
              <span class="close-item"><i class="fas fa-times"></i></span>
              <div class="thumb">
                <a onclick="openModal()"><img src="assets/images/products/cart/02.png" alt="products"></a>
              </div>
              <div class="product-content">
                <a href="product-detail.html" class="product-title">Daisy Cont Oil</a>
                <div class="product-cart-info">
                  1x 31b
                </div>
                <div class="product-price">
                  <del>$8.00</del><span class="ml-4">$5.00</span>
                </div>
                <div class="cart-btn-toggle" onclick="cartopen()">
                  <span class="cart-btn"><i class="fas fa-shopping-cart"></i> Cart</span>

                  <div class="price-btn">
                    <div class="price-increase-decrese-group d-flex">
                      <span class="decrease-btn">
                        <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">-
                        </button>
                      </span>
                      <input type="text" name="quantity" class="form-controls input-number" value="1">
                      <span class="increase">
                        <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">+
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="wishlist-item product-item d-flex align-items-center">
              <span class="close-item"><i class="fas fa-times"></i></span>
              <div class="thumb">
                <a onclick="openModal()"><img src="assets/images/products/cart/03.png" alt="products"></a>
              </div>
              <div class="product-content">
                <a href="product-detail.html" class="product-title">Daisy Cont Oil</a>
                <div class="product-cart-info">
                  1x 31b
                </div>
                <div class="product-price">
                  <del>$8.00</del><span class="ml-4">$5.00</span>
                </div>
                <div class="cart-btn-toggle" onclick="cartopen()">
                  <span class="cart-btn"><i class="fas fa-shopping-cart"></i> Cart</span>

                  <div class="price-btn">
                    <div class="price-increase-decrese-group d-flex">
                      <span class="decrease-btn">
                        <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">-
                        </button>
                      </span>
                      <input type="text" name="quantity" class="form-controls input-number" value="1">
                      <span class="increase">
                        <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">+
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="wishlist-item product-item d-flex align-items-center">
              <span class="close-item"><i class="fas fa-times"></i></span>
              <div class="thumb">
                <a onclick="openModal()"><img src="assets/images/products/cart/04.png" alt="products"></a>
              </div>
              <div class="product-content">
                <a href="product-detail.html" class="product-title">Daisy Cont Oil</a>
                <div class="product-cart-info">
                  1x 31b
                </div>
                <div class="product-price">
                  <del>$8.00</del><span class="ml-4">$5.00</span>
                </div>
                <div class="cart-btn-toggle" onclick="cartopen()">
                  <span class="cart-btn"><i class="fas fa-shopping-cart"></i> Cart</span>

                  <div class="price-btn">
                    <div class="price-increase-decrese-group d-flex">
                      <span class="decrease-btn">
                        <button type="button" class="btn quantity-left-minus" data-type="minus" data-field="">-
                        </button>
                      </span>
                      <input type="text" name="quantity" class="form-controls input-number" value="1">
                      <span class="increase">
                        <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">+
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- dashboard-section end -->


<?php include('inc/footer.php'); ?>