<div class="catagory-sidebar-area fixed-totop">
    <div class="catagory-sidebar-area-inner">
        <div class="catagory-sidebar all-catagory-option">
            <ul class="catagory-submenu">
                <li><a data-toggle="collapse" href="#catagory-widget1" role="button" aria-expanded="false" aria-controls="catagory-widget1">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/vegetables.svg" alt="icon"></span>Vegetables</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse show" id="catagory-widget1">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget2" role="button" aria-expanded="false" aria-controls="catagory-widget2">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/harvest.svg" alt="icon"></span>Fruits</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget2">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget3" role="button" aria-expanded="false" aria-controls="catagory-widget3">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/salad.svg" alt="icon"></span>Salads</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget3">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget4" role="button" aria-expanded="false" aria-controls="catagory-widget4">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/seafood.svg" alt="icon"></span>Fish & seafood</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget4">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget5" role="button" aria-expanded="false" aria-controls="catagory-widget5">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/natural.svg" alt="icon"></span>Fresh Meat</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget5">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget6" role="button" aria-expanded="false" aria-controls="catagory-widget6">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/natural.svg" alt="icon"></span>Health Products</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget6">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget7" role="button" aria-expanded="false" aria-controls="catagory-widget7">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/eggs.svg" alt="icon"></span>Butter & Eggs</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget7">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget8" role="button" aria-expanded="false" aria-controls="catagory-widget8">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/seasoning.svg" alt="icon"></span>Oils and Venegar</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget8">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget9" role="button" aria-expanded="false" aria-controls="catagory-widget9">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/refrigerator.svg" alt="icon"></span>Frozen Food</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget9">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
                <li><a data-toggle="collapse" href="#catagory-widget10" role="button" aria-expanded="false" aria-controls="catagory-widget10">
                        <div class="d-flex align-items-center"><span class="icon"><img src="assets/images/svg/honey.svg" alt="icon"></span>Jam & Honey</div><i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="catagory-submenu collapse" id="catagory-widget10">
                        <li><a href="product-list.html">Artichoke.</a></li>
                        <li><a href="product-list.html">Aubergine (eggplant).</a></li>
                        <li><a href="product-list.html">Asparagus.</a></li>
                        <li><a href="product-list.html">Broccoflower (a hybrid).</a></li>
                        <li><a href="product-list.html">Broccoli (calabrese).</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>