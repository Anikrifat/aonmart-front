<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aonmart Grocery Shop</title>
    <!-- <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png" /> -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="assets/css/slick-theme.css">
    <link rel="stylesheet" href="assets/css/custom-select.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/newstyle.css">
</head>

<body id="top-page">
    <a class="position-absolute" href="javascript:void(0)" onclick="cartopen()">
        <div id="sitebar-drawar" class="sitebar-drawar">
            <div class="cart-count d-flex align-items-center">
                <i class="fas fa-shopping-basket"></i>
                <span>3 Item</span>
            </div>
            <div class="total-price">$15.00</div>
        </div>
    </a>
    <!--=============== modal-area=================== -->
    <?php include('inc/modal.php'); ?>
    <!--=============== modal-area=================== -->



    <!-- =============sidebar-cart=============== -->
    <?php include('inc/cart.php'); ?>
    <!-- =============sidebar-cart=============== -->

    <!-- header section start -->
    <header class="header">
        <div class="header-top d-xl-none">

            <div class="mobile-header  fixed-totop">
                <!-- <a href="index.php" class="logo">
                    <h2 class="text-center">Aonmart</h2>
                </a> -->
                <div class="d-flex justify-content-between align-items-center">
                    <div class="all-catagory-option mobile-device">
                        <a class="bar-btn"><i class="fas fa-bars"></i></a>
                        <a class="close-btn"><i class="fas fa-bars"></i></a>
                    </div>


                    <!-- search select -->
                    <div class="text-center mobile-search">
                    <div class="select-search-option">
                
                    <form action="#" class="search-form">
                        <input type="text" name="search" placeholder="Search for Products">
                        <button class="submit-btn"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                        <!-- <button type="button" data-toggle="modal" data-target="#search-select-id"><i class="fas fa-search"></i></button> -->
                    </div>
                    <div class="text-center ">
                        <button type="button" class="map-marker" data-toggle="modal" data-target="#search-select-id"><img src="assets/images/map.gif" alt=""></button>
                    </div>
                    <!-- menubar -->
                    <!-- <div>
                        <a onclick="OpenSignUpForm()" href="#" type="button" class="menu-bar btn btn-light btn-sm text-center">
                            <i class="fas fa-user"></i>
                        </a>
                    </div> -->
                    <!-- <div>
                    <a onclick="OpenSignUpForm()" href="#" type="button" class="menu-bar btn btn-light btn-sm text-center">
                    <i class="fas fa-map-marker"></i>
                        </a>
                        <!-- <span></span> -->
                    </div> 
                </div>
            </div>
        </div>
        <!-- <div class="header-top">
            <div class="mobile-header d-flex justify-content-between align-items-center d-xl-none"> -->
        <!-- <div class="d-flex align-items-center">
                    <div class="all-catagory-option mobile-device">
                        <a class="bar-btn"><i class="fas fa-bars"></i></a>
                        <a class="close-btn"><i class="fas fa-times"></i></a>
                    </div>
                    <a href="index.html" class="logo"><h3>Aonmart</h3></a>
                </div> -->

        <!-- <div class="all-catagory-option mobile-device">
                    <a class="bar-btn"><i class="fas fa-bars"></i><span class="ml-2 d-none d-md-inline">All Catagories</span></a>
                    <a class="close-btn"><i class="fas fa-times"></i><span class="ml-2 d-none d-md-inline">All Catagories</span></a>
                </div>
                <a href="index.php" class="logo"><img src="assets/images/logo.png" alt="logo"></a> -->

        <!-- search select -->
        <!-- <div class="text-center mobile-search">
                    <button type="button" data-toggle="modal" data-target="#search-select-id"><i class="fas fa-search"></i></button>
                </div> -->

        <!-- menubar -->
        <!-- <div>
                    <button class="menu-bar" type="button" data-toggle="modal" data-target="#menu-id">
                        Home<i class="fas fa-caret-down"></i>
                    </button>
                </div>

            </div>
            <div class="d-none d-xl-flex row align-items-center">
                <div class="col-5 col-md-2">
                    <a href="index.php" class="logo"><img src="assets/images/logo.png" alt="logo"></a>
                </div>
                <div class="col-5 col-md-9 col-lg-5">
                   
                    <div class="select-search-option d-none d-md-flex">
                        <div class="flux-custom-select">
                            <select>
                              <option value="0">Select Catagory</option>
                              <option value="1">Vegetables</option>
                              <option value="2">Fruits</option>
                              <option value="3">Salads</option>
                              <option value="4">Fish & Seafood</option>
                              <option value="5">Fresh Meat</option>
                              <option value="6">Health Product</option>
                              <option value="7">Butter & Eggs</option>
                              <option value="8">Oils & Venegar</option>
                              <option value="9">Frozen Food</option>
                              <option value="10">Jam & Honey</option>
                            </select>
                        </div>
                        <form action="#" class="search-form">
                            <input type="text" name="search" placeholder="Search for Products">
                            <button class="submit-btn"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
               

            </div>
        </div> -->
        <!-- <hr> -->
        <div class="header-bottom fixed-totop d-none d-xl-block">
            <div class="row m-0 align-items-center mega-menu-relative">
                <div class="col-md-1 p-0 d-none d-xl-block">
                    <div class="all-catagory-option">
                        <a class="bar-btn"><i class="fas fa-bars"></i></a>
                        <a class="close-btn"><i class="fas fa-bars"></i></a>
                    </div>
                </div>
                <div class="col-md-3">
                    <a href="index.php">
                        <h2>AONMART</h2>
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="menu-area d-none d-xl-flex justify-content-between align-items-center">

                        <form class="form-inline top-search">
                            <input class="form-control mx-0 rounded-0" type="text" placeholder="Search">
                            <button class="btn btn-success rounded-0 mx-0" type="submit"><i class="fas fa-search"></i></button>
                        </form>
                        <ul class="site-action d-none d-lg-flex align-items-center justify-content-between  ml-auto">
                            <li class="my-account d-none"><a class="dropdown-toggle" href="#" role="button" id="myaccount" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user mr-1"></i> My Account</a>
                                <ul class="submenu dropdown-menu" aria-labelledby="myaccount">
                                    <li><a href="profile.html">Profile</a></li>
                                    <li><a href="#">Sign Out</a></li>
                                </ul>
                            </li>
                            <li>
                                <div class="flux-custom-select">
                                    <select>
                                        <option value="4">Select Locaion </option>
                                        <option value="1">Badda</option>
                                        <option value="2">Dhanmondi</option>
                                        <option value="3">Basundhara</option>

                                    </select>

                                </div>
                            </li>
                            <li class="signin-option rounded-0"><a onclick="OpenSignUpForm()" href="#"><i class="fas fa-user mr-2"></i>Sign In</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header section end -->
    <div class="page-layout  open-side-menu">
        <?php include('inc/sidebar.php'); ?>
        <div class="main-content-area">