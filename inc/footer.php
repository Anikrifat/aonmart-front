<!-- footer section -->
<footer class="footer">
    <div class="container">
        <div class="footer-newsletter">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-left mb-3 mb-md-0">
                    <div class="newsletter-heading">
                        <h5>Know it all first</h5>
                    </div>
                </div>
                <div class="col-md-6 d-flex justify-content-center justify-content-md-end">
                    <div class="newsletter-form">
                        <input type="text" name="email" placeholder="E-mail Address">
                        <button class="submit-btn">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                <path style="fill:#2196F3;" d="M511.189,259.954c1.649-3.989,0.731-8.579-2.325-11.627l-192-192 c-4.237-4.093-10.99-3.975-15.083,0.262c-3.992,4.134-3.992,10.687,0,14.82l173.803,173.803H10.667 C4.776,245.213,0,249.989,0,255.88c0,5.891,4.776,10.667,10.667,10.667h464.917L301.803,440.328 c-4.237,4.093-4.355,10.845-0.262,15.083c4.093,4.237,10.845,4.354,15.083,0.262c0.089-0.086,0.176-0.173,0.262-0.262l192-192 C509.872,262.42,510.655,261.246,511.189,259.954z" />
                                <path d="M309.333,458.546c-5.891,0.011-10.675-4.757-10.686-10.648c-0.005-2.84,1.123-5.565,3.134-7.571L486.251,255.88 L301.781,71.432c-4.093-4.237-3.975-10.99,0.262-15.083c4.134-3.992,10.687-3.992,14.82,0l192,192 c4.164,4.165,4.164,10.917,0,15.083l-192,192C314.865,457.426,312.157,458.546,309.333,458.546z" />
                                <path d="M501.333,266.546H10.667C4.776,266.546,0,261.771,0,255.88c0-5.891,4.776-10.667,10.667-10.667h490.667 c5.891,0,10.667,4.776,10.667,10.667C512,261.771,507.224,266.546,501.333,266.546z" />
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-top">
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <div class="footer-widget">
                        <a href="index-2.html" class="footer-logo">
                            <h1>Aonmart</h1>
                        </a>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod teincididunt ut labore et </p>
                        <ul class="social-media-list d-flex flex-wrap">
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                            <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- <div class="col-md-4 col-lg-4">
                    <div class="footer-widget">
                        <h5 class="footer-title">Product Catalog</h5>
                        <div class="widget-wrapper">
                            <ul>
                                <li><a href="product-leftsidebar.html">Fruits</a></li>
                                <li><a href="product-leftsidebar.html">Salads</a></li>
                                <li><a href="product-leftsidebar.html">Fish & Seafood</a></li>
                                <li><a href="product-leftsidebar.html">Fresh Meat</a></li>
                                <li><a href="product-leftsidebar.html">Health Products</a></li>
                                <li><a href="product-leftsidebar.html">Butter & Eggs</a></li>
                                <li><a href="product-leftsidebar.html">Oil & Vinegars</a></li>
                                <li><a href="product-leftsidebar.html">Health Products</a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <!-- <div class="col-md-4 col-lg-4">
                    <div class="footer-widget">
                        <h5 class="footer-title">Useful Links</h5>
                        <div class="widget-wrapper">
                            <ul>
                                <li><a href="about.html">About Us</a></li>
                                <li><a href="product-leftsidebar.html">Featured Products</a></li>
                                <li><a href="brand-product.html">Offers</a></li>
                                <li><a href="blog-rightsidebar.html">Blog</a></li>
                                <li><a href="faq.html">Faq</a></li>
                                <li><a href="contact.html">Careers</a></li>
                                <li><a href="contact.html">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->

                <div class="col-md-4 col-lg-4">
                    <div class="footer-widget">
                        <h5 class="footer-title">Download Apps</h5>
                        <div class="widget-wrapper">
                            <div class="apps-store">
                                <a href="#"><img src="assets/images/app-store/apple.png" alt="app"></a>
                                <a href="#"><img src="assets/images/app-store/google.png" alt="app"></a>
                            </div>
                            <!-- <div class="payment-method d-flex flex-wrap">
                                <a href="#"><img src="assets/images/payment/visa.png" alt="payment"></a>
                                <a href="#"><img src="assets/images/payment/paypal.png" alt="payment"></a>
                                <a href="#"><img src="assets/images/payment/master.png" alt="payment"></a>
                                <a href="#"><img src="assets/images/payment/discover.png" alt="payment"></a>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4">
                    <img src="assets/images/payment.png" class="w-100" alt="">
                </div>
            </div>
        </div>

        <div class="footer-bottom">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mb-3 mb-md-0">
                    <p class="copyright">Copyright &copy; 2021 <a href="#">Aonmart</a>. All Rights Reserved.</p>
                </div>

                <div class="col-md-6 d-flex justify-content-center justify-content-md-end">
                    <ul class="footer-menu d-flex flex-wrap">
                        <li><a href="#">Privacy policies</a></li>
                        <li><a href="#">Coockies</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer section -->
</div>
</div>


<!-- <div id="popup" class="popup" style="">
        <div class="popup-overlay"></div>
        <div class="popup-wrapper">
            <div class="popupbox">
                <div class="bg-success w-100 h-100">
                <div class="flux-custom-select">
                                    <select>
                                        <option value="4">Select Locaion </option>
                                        <option value="1">Badda</option>
                                        <option value="2">Dhanmondi</option>
                                        <option value="3">Basundhara</option>
                                      
                                    </select>
                                
                                </div>
                </div>
                <button class="popup-close"><img src="assets/images/popup-close.png" alt="popup-close"></button>
            </div>
        </div>
    </div> -->

<!-- product-details-popup start -->
<?php include('inc/product-details.php'); ?>
<!-- product-details-popup end -->


<!-- login-area -->
<section id="login-area" class="login-area">
    <div onclick="CloseSignUpForm()" class="overlay"></div>
    <div class="login-body-wrapper">
        <div class="login-body">
            <div class="close-icon" onclick="CloseSignUpForm()">
                <i class="fas fa-times"></i>
            </div>
            <div class="login-header">
                <h4>Create Your Account</h4>
                <p>Login with your email & password</p>
            </div>
            <div class="login-content">
                <form action="#" class="login-form">
                    <input type="text" name="name" placeholder="Name">
                    <input type="email" name="email" placeholder="Email">
                    <button type="submit" class="submit">Sign Up</button>
                </form>
                <div class="text-center seperator">
                    <span>Or</span>
                </div>
                <div class="othersignup-option">
                    <a class="facebook" href="#"><i class="fab fa-facebook-square"></i>Continue with Facebook</a>
                    <a class="google" href="#"><i class="fab fa-google-plus"></i>Continue with Google</a>
                </div>
                <div class="text-center dont-account py-4">
                    <p class="mb-0">Don't have any account <a href="register.php">Sing Up</a></p>
                </div>
            </div>
        </div>
        <div class="forgot-password text-center">
            <p>forgot Passowrd? <a href="#">Reset It</a></p>
        </div>
    </div>
</section>
<!-- login-area -->


<!-- mobile-footer -->
<div class="mobile-footer d-flex justify-content-between align-items-center d-xl-none">
    <button class="info" type="button" data-toggle="modal" data-target="#siteinfo1"><i class="fas fa-info-circle"></i></button>

    <div class="footer-cart">
        <a onclick="cartopen()" href="#" class="d-flex align-items-center"><span class="cart-icon"><i class="fas fa-shopping-cart"></i><span class="count">3</span></span> <span class="cart-amount ml-2">$560.00</span></a>
    </div>

    <div class="footer-admin-area">
        <!-- <span class="user-admin">
                <i class="fas fa-user"></i>
            </span> -->
        <button class="user-admin" type="button" data-toggle="modal" data-target="#useradmin1"><i class="fas fa-user"></i></button>
    </div>
</div>
<!-- mobile-footer -->


<a href="#top-page" class="to-top js-scroll-trigger"><span><i class="fas fa-arrow-up"></i></span></a>
<script src='assets/js/jquery.min.js'></script>
<script src='assets/js/bootstrap.bundle.min.js'></script>
<script src='assets/js/swiper.min.js'></script>
<script src="assets/js/slick.js"></script>
<script src='assets/js/jquery-easeing.min.js'></script>
<script src='assets/js/scroll-nav.js'></script>
<script src="assets/js/jquery.elevatezoom.js"></script>
<script src='assets/js/price-range.js'></script>
<script src='assets/js/custom-select.js'></script>
<script src='assets/js/multi-countdown.js'></script>
<script src='assets/js/fly-cart.js'></script>
<script src='assets/js/theia-sticky-sidebar.js'></script>
<script src='assets/js/functions.js'></script>

</body>

</html>