<!-- address-modal -->
<!-- menu modal -->
<div class="modal fade address-edit-box" id="address-edit" tabindex="-1" aria-labelledby="address-edit" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Edit Your Address</h4>
                <form action="#" class="address-form">
                    <!-- <div class="input-item">
                            <label>Address Type</label>
                            <input type="text" name="name" placeholder="Home">
                        </div> -->
                    <div class="input-item">
                        <label>Address</label>
                        <input type="text" name="name" placeholder="2548 Broaddus Maple Court Avenue">
                    </div>
                    <div class="input-item">
                        <label>City</label>
                        <div class="flux-custom-select">
                            <select>
                                <option value="0">Alberta</option>
                                <option value="1">British Columbia</option>
                                <option value="2">Manitoba</option>
                                <option value="3">New Brunswick</option>
                                <option value="4">Nova Scotia</option>
                                <option value="5">Ontario</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-item">
                        <label>State</label>
                        <!-- <input type="text" name="name" placeholder="Sun fransico"> -->
                        <div class="flux-custom-select">
                            <select>
                                <option value="0">Nunavut</option>
                                <option value="1">Northwest Territories</option>
                                <option value="2"> Ontario</option>
                                <option value="3">British Columbia</option>
                                <option value="4">Nova Scotia</option>
                                <option value="5">Prince Edward Island</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-item">
                        <label>zip</label>
                        <input type="text" name="name" placeholder="9847">
                    </div>
                    <div class="input-item">
                        <label>Country</label>
                        <!-- <input type="text" name="name" placeholder="USA"> -->
                        <div class="flux-custom-select">
                            <select>
                                <option value="0">Canada</option>
                                <option value="1">USA</option>
                                <option value="2"> UK</option>
                                <option value="3">Spain</option>
                                <option value="4">Italy</option>
                                <option value="5">Portgal</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <button class="submit">Save</button>
                        <button data-dismiss="modal" class="cancel">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- menu modal -->
<div class="modal fade address-edit-box" id="address-add" tabindex="-1" aria-labelledby="address-add" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Add Your Address</h4>
                <form action="#" class="address-form">
                    <div class="input-item">
                        <label>Address Type</label>
                        <input type="text" name="name" placeholder="Home">
                    </div>
                    <div class="input-item">
                        <label>Address</label>
                        <input type="text" name="name" placeholder="2548 Broaddus Maple Court Avenue">
                    </div>
                    <div class="input-item">
                        <label>City</label>
                        <div class="flux-custom-select">
                            <select>
                                <option value="0">Alberta</option>
                                <option value="1">British Columbia</option>
                                <option value="2">Manitoba</option>
                                <option value="3">New Brunswick</option>
                                <option value="4">Nova Scotia</option>
                                <option value="5">Ontario</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-item">
                        <label>State</label>
                        <!-- <input type="text" name="name" placeholder="Sun fransico"> -->
                        <div class="flux-custom-select">
                            <select>
                                <option value="0">Nunavut</option>
                                <option value="1">Northwest Territories</option>
                                <option value="2"> Ontario</option>
                                <option value="3">British Columbia</option>
                                <option value="4">Nova Scotia</option>
                                <option value="5">Prince Edward Island</option>
                            </select>
                        </div>
                    </div>
                    <div class="input-item">
                        <label>zip</label>
                        <input type="text" name="name" placeholder="9847">
                    </div>
                    <div class="input-item">
                        <label>Country</label>
                        <!-- <input type="text" name="name" placeholder="USA"> -->
                        <div class="flux-custom-select">
                            <select>
                                <option value="0">Canada</option>
                                <option value="1">USA</option>
                                <option value="2"> UK</option>
                                <option value="3">Spain</option>
                                <option value="4">Italy</option>
                                <option value="5">Portgal</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <button class="submit">Save</button>
                        <button data-dismiss="modal" class="cancel">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- address-modal-end -->


<!-- profile-edit form -->
<div class="profile-edit modal fade" id="edit-form1" tabindex="-1" aria-labelledby="edit-form1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="profile-edit-container">
                <div class="head text-center">
                    <h4 class="title">Edit Your Profile</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas rutrum, <br>augue sit amet ullamcorper mattis</p>
                </div>
                <form action="#" class="profile-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-item">
                                <label>Your Name</label>
                                <input type="text" name="name" placeholder="Jhone Doe">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-item">
                                <label>Your Email</label>
                                <input type="text" name="email" placeholder="jhondoe@gmail.com">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-item">
                                <label>Your Website</label>
                                <input type="text" name="website" placeholder="jhondoe.com">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-item">
                                <label>Your Mobile</label>
                                <input type="text" name="mobile" placeholder="0001111222">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-item">
                                <label>City</label>
                                <div class="flux-custom-select">
                                    <select>
                                        <option value="0">Alberta</option>
                                        <option value="1">British Columbia</option>
                                        <option value="2">Manitoba</option>
                                        <option value="3">New Brunswick</option>
                                        <option value="4">Nova Scotia</option>
                                        <option value="5">Ontario</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-item">
                                <label>State</label>
                                <div class="flux-custom-select">
                                    <select>
                                        <option value="0">Nunavut</option>
                                        <option value="1">Northwest Territories</option>
                                        <option value="2"> Ontario</option>
                                        <option value="3">British Columbia</option>
                                        <option value="4">Nova Scotia</option>
                                        <option value="5">Prince Edward Island</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-item">
                                <label>Zip Code</label>
                                <input type="text" name="zipcode" placeholder="94066">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-item">
                                <label>Country</label>
                                <div class="flux-custom-select">
                                    <select>
                                        <option value="0">Canada</option>
                                        <option value="1">USA</option>
                                        <option value="2"> UK</option>
                                        <option value="3">Spain</option>
                                        <option value="4">Italy</option>
                                        <option value="5">Portgal</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="input-item">
                                <label>White About Your Self</label>
                                <textarea></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type='submit' class="submit">Save</button>
                            <button class="cencel" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- profile-edit form -->
<!-- admin Modal -->
<div class="modal fade" id="useradmin1" tabindex="-1" aria-labelledby="useradmin1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="header-top-action-dropdown">
                    <ul>
                        <li class="signin-option"><a onclick="OpenSignUpForm()" href="#" data-dismiss="modal"><i class="fas fa-user mr-2"></i>Sign In</a></li>
                        <li class="site-phone"><a href="tel:000-000-000"><i class="fas fa-phone"></i> 000 000 000</a></li>
                        <li class="site-help"><a href="#"><i class="fas fa-question-circle"></i> Help & More</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--siteinfo Modal -->
<div class="modal fade" id="siteinfo1" tabindex="-1" aria-labelledby="siteinfo1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="header-top-action-dropdown">
                    <ul>
                        <li class="site-phone"><a href="tel:000-000-000"><i class="fas fa-phone"></i> 000 000 000</a></li>
                        <li class="site-help"><a href="#"><i class="fas fa-question-circle"></i> Help & More</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--search Modal -->
<div class="modal fade" id="search-select-id" tabindex="-1" aria-labelledby="search-select-id" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="select-search-option">
                
                    <form action="#" class="search-form">
                        <input type="text" name="search" placeholder="Search for Products">
                        <button class="submit-btn"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- menu modal -->
<div class="modal fade" id="menu-id" tabindex="-1" aria-labelledby="menu-id" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <ul class="menu d-xl-flex flex-wrap pl-0 list-unstyled">
                    <li class="item-has-children"><a data-toggle="collapse" href="#mainmenuid1" role="button" aria-expanded="false" aria-controls="catagory-widget1"><span>Home</span> <i class="fas fa-angle-down"></i></a>
                        <ul class="submenu collapse" id="mainmenuid1">
                            <li><a href="home-default.html">Home Default</a></li>
                            <li><a href="index-icon.html">Home Default2</a></li>
                            <li><a href="index-2.html">Home Sticky Sidebar</a></li>
                            <li><a href="home-search.html">Home Search</a></li>
                            <li><a href="home-slider.html">Home Slider</a></li>
                            <li><a href="home-slider2.html">Home Slider2</a></li>
                            <li><a href="home7.html">Home Seven</a></li>
                        </ul>
                    </li>
                    <li><a href="#">New Products</a></li>
                    <li><a data-toggle="collapse" href="#megamenu-main" role="button" aria-expanded="false" aria-controls="catagory-widget1"><span>Featured Products</span> <i class="fas fa-angle-down"></i></a>
                        <ul class=" collapse" id="megamenu-main">
                            <li><a data-toggle="collapse" href="#megamenu-main01" role="button" aria-expanded="false" aria-controls="megamenu-main01"><span>Vegetables</span> <i class="fas fa-angle-down"></i></a>
                                <ul class="submenu collapse" id="megamenu-main01">
                                    <li><a href="product-list.html">Artichoke.</a></li>
                                    <li><a href="product-list.html">Aubergune</a></li>
                                    <li><a href="product-list.html">Asparagus</a></li>
                                    <li><a href="product-list.html">Broccoflower</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" href="#megamenu-main02" role="button" aria-expanded="false" aria-controls="megamenu-main02"><span>Fruits</span> <i class="fas fa-angle-down"></i></a>
                                <ul class="submenu collapse" id="megamenu-main02">
                                    <li><a href="product-list.html">Artichoke.</a></li>
                                    <li><a href="product-list.html">Aubergune</a></li>
                                    <li><a href="product-list.html">Asparagus</a></li>
                                    <li><a href="product-list.html">Broccoflower</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" href="#megamenu-main03" role="button" aria-expanded="false" aria-controls="megamenu-main03"><span>Salads</span> <i class="fas fa-angle-down"></i></a>
                                <ul class="submenu collapse" id="megamenu-main03">
                                    <li><a href="product-list.html">Artichoke.</a></li>
                                    <li><a href="product-list.html">Aubergune</a></li>
                                    <li><a href="product-list.html">Asparagus</a></li>
                                    <li><a href="product-list.html">Broccoflower</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" href="#megamenu-main04" role="button" aria-expanded="false" aria-controls="megamenu-main04"><span>Health Care</span> <i class="fas fa-angle-down"></i></a>
                                <ul class="submenu collapse" id="megamenu-main04">
                                    <li><a href="product-list.html">Artichoke.</a></li>
                                    <li><a href="product-list.html">Aubergune</a></li>
                                    <li><a href="product-list.html">Asparagus</a></li>
                                    <li><a href="product-list.html">Broccoflower</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="item-has-children"><a data-toggle="collapse" href="#mainmenuid2" role="button" aria-expanded="false" aria-controls="mainmenuid2"><span>Pages</span> <i class="fas fa-angle-down"></i></a>
                        <ul class="submenu collapse" id="mainmenuid2">
                            <li><a href="about.html">About</a></li>
                            <li><a href="contact.html">Contact</a></li>
                            <li class="item-has-children"><a data-toggle="collapse" href="#mobile-product1" role="button" aria-expanded="false" aria-controls="mobile-product1"><span>Products</span> <i class="fas fa-angle-down"></i></a>
                                <ul class="submenu collapse" id="mobile-product1">
                                    <li><a href="product-list.html">Product List</a></li>
                                    <li><a href="product-leftsidebar.html">Product leftsidebar</a></li>
                                    <li><a href="product-fullwidth.html">Product Fullwidth</a></li>
                                    <li><a href="brand-product.html">Brand Page</a></li>
                                    <li><a href="product-detail.html">Product Details</a></li>
                                </ul>
                            </li>
                            <li class="item-has-children"><a data-toggle="collapse" href="#mobile-dashboard1" role="button" aria-expanded="false" aria-controls="mobile-dashboard1"><span>Dashboard1</span> <i class="fas fa-angle-down"></i></a>
                                <ul class="submenu collapse" id="mobile-dashboard1">
                                    <li><a href="user-dashbord.html">User Dashboard</a></li>
                                    <li><a href="profile.html">Profile</a></li>
                                    <li><a href="track-order.html">Track Order</a></li>
                                    <li><a href="wishlist.html">Wish List</a></li>
                                </ul>
                            </li>
                            <li class="item-has-children"><a data-toggle="collapse" href="#mobile-dashboard2" role="button" aria-expanded="false" aria-controls="mobile-dashboard2"><span>Dashboard2</span> <i class="fas fa-angle-down"></i></a>
                                <ul class="submenu collapse" id="mobile-dashboard2">
                                    <li><a href="dashboard.html">My Orders</a></li>
                                    <li><a href="dashboard-account.html">Accounts</a></li>
                                    <li><a href="dashboard-address-book.html">Address Book</a></li>
                                    <li><a href="dashboard-order-cancel.html">Order Cancel</a></li>
                                    <li><a href="dashboard-order-process.html">Order Process</a></li>
                                    <li><a href="dashboard-password-change.html">Password Change</a></li>
                                    <li><a href="dashboard-wishlist.html">whistlist</a></li>
                                </ul>
                            </li>
                            <li><a href="faq.html">FAQ</a></li>
                            <li><a href="checkout.html">Checkout</a></li>
                            <li><a href="checkout.html">Checkout</a></li>
                            <li><a href="singin.html">SignIn</a></li>
                            <li><a href="signup.html">SignUp</a></li>
                            <li><a href="product-order-success.html">Order Success</a></li>
                            <li><a href="comming-soon.html">Comming Soon</a></li>
                            <li><a href="404-page.html">404 page</a></li>
                        </ul>
                    </li>
                    <li class="item-has-children"><a data-toggle="collapse" href="#mainmenuid3" role="button" aria-expanded="false" aria-controls="mainmenuid3"><span>Blog</span> <i class="fas fa-angle-down"></i></a>
                        <ul class="submenu collapse" id="mainmenuid3">
                            <li><a href="blog.html">Blog full width</a></li>
                            <li><a href="blog-rightsidebar.html">Blog Rightsidebar</a></li>
                            <li><a href="single.html">Blog Single</a></li>
                        </ul>
                    </li>
                    <li><a href="contact.html">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>